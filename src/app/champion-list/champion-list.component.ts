import { Component, OnInit, Input } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Champion } from '../Champion';
import { RiotService } from '../services/riot.service'

@Component({
  selector: 'app-champion-list',
  templateUrl: './champion-list.component.html',
  styleUrls: ['./champion-list.component.css']
})
export class ChampionListComponent implements OnInit {
  @Input() champions : Champion[] = [];

  constructor() { }

  ngOnInit(): void {}

}
