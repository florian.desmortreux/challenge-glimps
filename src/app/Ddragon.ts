import { Champion } from "./Champion";

export interface Ddragon {
    type: string,
    format: string,
    version: string
    data : { [key: string] : Champion }
}