import { TestBed } from '@angular/core/testing';

import { RiotService } from './riot.service';
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('RiotService', () => {
  let service: RiotService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(RiotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
