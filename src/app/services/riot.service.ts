import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { map, Observable, of, forkJoin } from 'rxjs';
import { environment } from '../../environments/environment';
import { Champion } from '../Champion'
import { Ddragon } from '../Ddragon'
import { ChampionInfo } from '../ChampionInfo';

@Injectable({
  providedIn: 'root'
})
export class RiotService {
  constructor(private http : HttpClient) {}

  getChampions() : Observable<{ [key: string] : Champion }> {
    return this.http.get<Ddragon>(environment.ddragon.dataUrl.champions(environment.default_lang)).pipe(map((ddragon) => {     
      return ddragon.data;
    }));
  }

  getChampionInfo() : Observable<ChampionInfo> {
    let headers : HttpHeaders = new HttpHeaders({ "X-Riot-Token": environment.riot.v3.token, });
    return this.http.get<ChampionInfo>(environment.riot.v3.apiUrl, { headers: headers });
  }

  getChampionsById() : Observable<{ [key : string] : Champion }> {
    return this.getChampions().pipe(map((champions) => {
      let championByKey : { [key: string] : Champion }= {};

      for (let champion of Object.values(champions)) {
        championByKey[champion.key] = champion;
      }

      return championByKey;
    }));
  }

  getFreeChampion() : Observable<Champion[]> {
    return forkJoin([
      this.getChampionInfo(),
      this.getChampionsById()
    ])
    .pipe(map(([championInfo, champions]) => {   
      return championInfo.freeChampionIds.map((id) => champions[id]);
    }));
  }
}
