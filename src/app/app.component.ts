import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { RiotService } from './services/riot.service';
import { ChampionInfo } from './ChampionInfo';
import { Champion } from './Champion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = "glimps-challenge"
  champions : Observable<Champion[]>;
  
  constructor(private riotService : RiotService) { 
    // Display all champions
    // this.champions = this.riotService.getChampions().pipe<Champion[]>(map((champions) => { return Object.values(champions) }));
    
    // Display free champions of the week
    this.champions = this.riotService.getFreeChampion();
  }

  ngOnInit(): void {
    
  }

}
