import { Component, OnInit, Input, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Champion } from '../Champion'
import { environment } from '../../environments/environment';
import { RiotService } from '../services/riot.service';

@Component({
  selector: 'app-champion-item',
  templateUrl: './champion-item.component.html',
  styleUrls: ['./champion-item.component.css']
})
export class ChampionItemComponent implements OnInit, AfterViewInit {
  @Input() champion? : Champion
  champ_img? : string
  @ViewChild('p') p! : ElementRef
  showBlurb : boolean = false;

  constructor(private riotService : RiotService) { }

  ngOnInit(): void {
    if (this.champion)
      this.champ_img = environment.ddragon.imgUrl.champion(this.champion.image.full);
  }
  
  ngAfterViewInit() {
    this.p.nativeElement .style.height = '0px';
  }

  getSrc(name: string): string {
    return `/assets/${name.toLowerCase()}.png`;
  }

  toggleBlurb(): void {
    this.showBlurb = !this.showBlurb;
    let element = this.p.nativeElement;
    if (this.showBlurb) element.style.height = element.scrollHeight + 'px';
    else element.style.height = '0px';
  }
}
