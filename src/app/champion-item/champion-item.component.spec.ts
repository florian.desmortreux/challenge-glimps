import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChampionItemComponent } from './champion-item.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ChampionItemComponent', () => {
  let component: ChampionItemComponent;
  let fixture: ComponentFixture<ChampionItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ ChampionItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChampionItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
