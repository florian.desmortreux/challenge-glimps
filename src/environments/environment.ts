// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  default_lang : 'fr_FR',
  riot: {
    v3 : {
      apiUrl : '/api/lol/platform/v3/champion-rotations',
      token: 'RGAPI-e4a78e1b-fdc6-4f7d-9a2a-c94dadfc292c',
    }
  },
  ddragon : {
    dataUrl: {
      champions : (lang: string) => `http://ddragon.leagueoflegends.com/cdn/12.6.1/data/${lang}/champion.json`,
    },
    imgUrl : {
      champion: (name: string) => `https://ddragon.leagueoflegends.com/cdn/12.6.1/img/champion/${name}`,
    } 
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
